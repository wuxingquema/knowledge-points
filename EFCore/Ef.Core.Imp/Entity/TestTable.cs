﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ef.Core.Imp.Entity
{
    [Table("TestTable")]
    public class TestTable : EntityBase
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        //关联一对多的导航属性
        public virtual ICollection<TestTableDetail> TestTableDetails { get; set; }

        //关联一对一的导航属性
        public virtual TestTableRelation TestTableRelation { get; set; }
    }

    [Table("TestTableRelation")]
    public class TestTableRelation : EntityBase
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public int PID { get; set; }        
    }

    [Table("TestTableDetail")]
    public class TestTableDetail : EntityBase
    {
        [Key]
        public int Id { get; set; }
        public int PID { get; set; }
        public string Name { get; set; }
        public virtual TestTable Test { get; set; }
    }

    public class EntityBase
    {

    }    
}
