﻿using System.Collections.Generic;
using System.Linq;
using Ef.Core.Imp;
using Ef.Core.Imp.Entity;
using Microsoft.EntityFrameworkCore;

namespace Ef.Core
{
    public class MyDbContextTest
    {
        #region 新增测试

        public static async void Insert_测试新增数据1()
        {
            var myDbContext = new MyDbContext();

            if (myDbContext.TestTables.Any(p => p.Id == 1)) return;

            var newEntity = new TestTable
            {
                Id = 1,
                Name = "主表数据1"
            };

            await myDbContext.TestTables.AddAsync(newEntity);

            myDbContext.SaveChanges();

            System.Console.WriteLine($"TestTable Insert Success");
            System.Console.WriteLine($"------------------------");
        }

        public static async void Insert_测试新增数据2()
        {
            var myDbContext = new MyDbContext();

            if (myDbContext.TestTables.Any(p => p.Id == 2)) return;

            var newEntity = new TestTable
            {
                Id = 2,
                Name = "主表数据2"
            };

            await myDbContext.TestTables.AddAsync(newEntity);

            myDbContext.SaveChanges();

            System.Console.WriteLine($"TestTable Insert Success");
            System.Console.WriteLine($"------------------------");
        }

        public static async void Insert_测试缓存机制_同一上下文()
        {
            await using (var myDbContext = new MyDbContext())
            {
                if (myDbContext.TestTables.Any(p => p.Id == 3)) return;

                var newEntity = new TestTable
                {
                    Id = 3,
                    Name = "主表数据3"
                };

                await myDbContext.TestTables.AddAsync(newEntity);

                var entity3 = myDbContext.TestTables.ToList().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.AsQueryable().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.Find(3);
            }

            System.Console.WriteLine($"------------------------");
        }

        public static async void Insert_测试缓存机制_不同一上下文()
        {
            await using (var myDbContext = new MyDbContext())
            {
                if (myDbContext.TestTables.Any(p => p.Id == 3)) return;

                var newEntity = new TestTable
                {
                    Id = 3,
                    Name = "主表数据3"
                };

                await myDbContext.TestTables.AddAsync(newEntity);

                var entity3 = myDbContext.TestTables.ToList().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.AsQueryable().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.Find(3);
            }

            await using (var myDbContext = new MyDbContext())
            {
                var entity3 = myDbContext.TestTables.ToList().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.AsQueryable().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.Find(3);
            }

            System.Console.WriteLine($"------------------------");
        }

        #endregion



        #region 删除测试

        public static async void Delete_跟踪查询_指定条件删除数据_ID为1()
        {
            var myDbContext = new MyDbContext();
            var entity = myDbContext.TestTables.FirstOrDefault(p => p.Id == 1);
            if (entity != null)
                myDbContext.TestTables.Remove(entity);
            myDbContext.SaveChanges();
        }

        public static async void Delete_非跟踪查询_指定条件删除数据_ID为1()
        {
            var myDbContext = new MyDbContext();
            var entity = myDbContext.TestTables.AsNoTracking().FirstOrDefault(p => p.Id == 1);
            if (entity != null)
                myDbContext.TestTables.Remove(entity);
            myDbContext.SaveChanges();
        }

        public static async void Delete_非查询对象_指定条件删除数据_ID为1()
        {
            var myDbContext = new MyDbContext();
            var entity = new TestTable()
            {
                Id = 1
            };
            myDbContext.TestTables.Remove(entity);
            myDbContext.SaveChanges();
        }

        #endregion


        #region 修改数据

        public static void Insert_新增数据1()
        {
            var myDbContext = new MyDbContext();

            if (myDbContext.TestTables.Any(p => p.Id == 1)) return;

            var newEntity = new TestTable
            {
                Id = 1,
                Name = "主表数据1"
            };

            myDbContext.TestTables.Add(newEntity);

            myDbContext.SaveChanges();

            System.Console.WriteLine($"TestTable Insert Success");
            System.Console.WriteLine($"------------------------");
        }

        public static void Insert_新增数据2()
        {
            var myDbContext = new MyDbContext();

            if (myDbContext.TestTables.Any(p => p.Id == 2)) return;

            var newEntity = new TestTable
            {
                Id = 2,
                Name = "主表数据2"
            };

            myDbContext.TestTables.Add(newEntity);

            myDbContext.SaveChanges();

            System.Console.WriteLine($"TestTable Insert Success");
            System.Console.WriteLine($"------------------------");
        }

        public static async void Insert_数据新增_缓存()
        {
            await using (var myDbContext = new MyDbContext())
            {
                if (myDbContext.TestTables.Any(p => p.Id == 3)) return;

                var newEntity = new TestTable
                {
                    Id = 3,
                    Name = "主表数据3"
                };

                await myDbContext.TestTables.AddAsync(newEntity);

                var entity3 = myDbContext.TestTables.ToList().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.AsQueryable().FirstOrDefault(p => p.Id == 3);

                entity3 = myDbContext.TestTables.Find(3);
            }

            System.Console.WriteLine($"------------------------");
        }

        #endregion


        #region 查询数据

        public static void Query_查询数据_全量查询()
        {
            var myDbContext = new MyDbContext();
            var list = myDbContext.TestTables.ToList();
            System.Console.WriteLine($"TestTable Count: {list.Count}");
            if (!list.Any()) return;
            System.Console.WriteLine($"TestTable Detail ----------------  ");
            foreach (var item in list)
            {
                System.Console.WriteLine($"ID : {item.Id} , Name : {item.Name}");
            }
            System.Console.WriteLine($"------------------------");
        }

        public static void Query_跟踪查询_修改数据()
        {
            var myDbContext = new MyDbContext();
            var list = myDbContext.TestTables.AsTracking().ToList();

            var firstEntity = list.FirstOrDefault(p => p.Id == 1);
            if (firstEntity != null) firstEntity.Name = $"{firstEntity.Name} Query_跟踪查询";

            myDbContext.SaveChanges();

            System.Console.WriteLine($"------------------------");
        }

        public static void Query_非跟踪查询_修改数据()
        {
            var myDbContext = new MyDbContext();
            var list = myDbContext.TestTables.AsNoTracking().ToList();

            var firstEntity = list.FirstOrDefault(p => p.Id == 1);
            if (firstEntity != null) firstEntity.Name = $"{firstEntity.Name} Query_非跟踪查询";

            myDbContext.SaveChanges();

            System.Console.WriteLine($"------------------------");
        }

        #endregion


        #region 关联业务处理查询

        public static void Query_内存查询()
        {
            TestTable newTable = new TestTable();
            newTable.Id = 10;
            newTable.Name = "测试数据";
            using (MyDbContext dbContext = new MyDbContext())
            {
                dbContext.Add(newTable);
                Query_内存查询_关联业务处理(dbContext);
            }
        }
        private static void Query_内存查询_关联业务处理(MyDbContext dbContext)
        {
            var entity = dbContext.TestTables.FirstOrDefault(p => p.Id == 10);
            //处理业务逻辑
            //...
            var entity2 = dbContext.TestTables.Find(10);
            //处理业务逻辑
            //...
        }

        #endregion


        #region 导航属性

        public static void Insert_导航属性_数据准备()
        {
            TestTable table = new TestTable();
            table.Id = 10;
            table.Name = "主表数据10";
            TestTableDetail detail1 = new TestTableDetail();
            detail1.Id = 1;
            //detail1.PID = 10;
            detail1.Name = "主表数据10-从表数据1";
            TestTableDetail detail2 = new TestTableDetail();
            detail2.Id = 2;
            //detail2.PID = 10;
            detail2.Name = "主表数据10-从表数据2";
            table.TestTableDetails = new List<TestTableDetail>();
            //table.TestTableDetails.Add(detail1);
            //table.TestTableDetails.Add(detail2);
            using (MyDbContext db = new MyDbContext())
            {
                if (db.TestTables.FirstOrDefault(p => p.Id == 10) != null)
                    return;
                db.TestTables.Add(table);
                db.TestTableDetails.Add(detail1);
                db.TestTableDetails.Add(detail2);
                db.SaveChanges();
            }
        }

        public static void Query_导航属性()
        {
            MyDbContext dbContext = new MyDbContext();
            //普通方式
            //var test1 = dbContext.TestTables.FirstOrDefault(p => p.Id == 10);
            //var test2 = dbContext.TestTables.Where(p => p.Id == 10)                
            //    .Include(c=>c.TestTableDetails)
            //    .FirstOrDefault();

            //分段查询方式
            ////定义查询条件，并不会执行数据库查询
            //var query = dbContext.TestTables.Where(p => p.Id == 10);
            ////执行查询，但是只会查询主表数据         
            //var test4 = query.FirstOrDefault();
            ////需要从表数据时，再触发查询
            //query.SelectMany(p => p.TestTableDetails).Load();

            //Join方式
            //var query = dbContext.TestTables.Where(p => p.Id == 10).Join(dbContext.TestTableDetails, a => a.Id, b => b.PID, (a, b) => new { a.Id, b.Name });
            //var test5 = query.FirstOrDefault();

            //延迟加载
            var test1 = dbContext.TestTables.FirstOrDefault(p => p.Id == 10);
            var count = test1.TestTableDetails.Count();
        }

        #endregion

        #region 关联数据更新

        public static void Insert_关联实体更新()
        {
            //首先新增一个一对一的数据
            using (MyDbContext db = new MyDbContext())
            {
                if (db.TestTables.FirstOrDefault(p => p.Id == 11) == null)
                {
                    TestTable testTable = new TestTable();
                    testTable.Id = 11;
                    testTable.Name = "测试1对一关联更新场景";
                    TestTableRelation relation = new TestTableRelation();
                    relation.Id = 1;
                    relation.Name = "测试1对一关联更新场景 - 从表数据1";
                    testTable.TestTableRelation = relation;
                    db.TestTables.Add(testTable);
                    db.SaveChanges();
                }
            }
            //将TestTableRelation于原TestTable关系切断
            using (MyDbContext db = new MyDbContext())
            {
                //1.将原数据查出
                var testTable = db.TestTables.Where(p => p.Id == 11)
                             .Include(p => p.TestTableRelation)
                             .FirstOrDefault();
                //2.准备新TestTable
                var testTable2 = new TestTable();
                testTable2.Id = 12;
                testTable2.Name = "测试1对一关联更新场景 12";

                //更新TestTableRelation与新TestTable关联
                testTable2.TestTableRelation = testTable.TestTableRelation;
                db.TestTables.Add(testTable2);

                //自己跟踪状态
                var entry = db.ChangeTracker.Entries<TestTable>().FirstOrDefault(entity => entity.Entity == testTable2);
                var imageState = entry.State;

                var entry2 = db.ChangeTracker.Entries<TestTableRelation>().FirstOrDefault(entity => entity.Entity == testTable2.TestTableRelation);
                var entry2State = entry2.State;
               
                db.SaveChanges();
            }
        }

        #endregion
    }
}
