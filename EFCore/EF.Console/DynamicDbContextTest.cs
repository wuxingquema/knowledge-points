﻿using System;
using System.Linq;
using Ef.Core.Imp;
using Ef.Core.Imp.Entity;

namespace Ef.Core
{
    public class DynamicDbContextTest
    {
        public static void Query_查询数据_全量查询()
        {
            var myDbContext = new DynamicDbContext();
            var list = myDbContext.Set<TestTable>().ToList();
            Console.WriteLine($"TestTable Count: {list.Count}");
            if (!list.Any()) return;
            Console.WriteLine($"TestTable Detail ----------------  ");
            foreach (var item in list)
            {
                Console.WriteLine($"ID : {item.Id} , Name : {item.Name}");
            }
            Console.WriteLine($"------------------------");
        }
    }
}
