﻿using System;

namespace Ef.Core
{
    class Program
    {
        static void Main(string[] args)
        {

            //DynamicDbContextTest.Query_查询数据_全量查询();

            MyDbContextTest.Query_查询数据_全量查询();

            MyDbContextTest.Insert_测试新增数据1();

            MyDbContextTest.Insert_测试新增数据2();

            MyDbContextTest.Delete_非查询对象_指定条件删除数据_ID为1();

            //MyDbContextTest.Delete_非跟踪查询_指定条件删除数据_ID为1();

            //MyDbContextTest.Delete_跟踪查询_指定条件删除数据_ID为1();

            MyDbContextTest.Query_查询数据_全量查询();

            MyDbContextTest.Query_查询数据_全量查询();

            MyDbContextTest.Query_非跟踪查询_修改数据();

            MyDbContextTest.Insert_测试缓存机制_同一上下文();

            MyDbContextTest.Insert_测试缓存机制_不同一上下文();


            MyDbContextTest.Query_内存查询();

            MyDbContextTest.Insert_导航属性_数据准备();

            MyDbContextTest.Query_导航属性();


            MyDbContextTest.Insert_关联实体更新();

            Console.Read();
        }
    }
}
